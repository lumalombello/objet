/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

import java.util.Random;

/**
 * @author luma
 */

public class Mage extends Personnage implements Combattant{
    
    /**
     * Constructeur de la classe Mage
     * @param nom                   nom du Mage
     * @param ptVie                 points de vie 
     * @param ptMana                points de mana 
     * @param pourcentageAtt        pourcentage d'attaque
     * @param pourcentagePar        pourcentage de parade
     * @param pourcentageMag        pourcentage de magie
     * @param pourcentageResistMag  pourcentage de résistance magique
     * @param degAtt                dégât d'ataque
     * @param degMag                dégât de magie
     * @param distAttMax            distance d'attaque maxime
     * @param pos                   position
     * @param ptPar                 points de parade du défenseur
     * @param degSub                dégâts subis
     */
    
    public Mage(String nom, int ptVie, int ptMana, int pourcentageAtt, int pourcentagePar, int pourcentageMag, int pourcentageResistMag, int degAtt, int degMag, int distAttMax, int ptPar, int degSub, Point2D pos) {
        super(nom, ptVie, ptMana, pourcentageAtt, pourcentagePar, pourcentageMag, pourcentageResistMag, degAtt, degMag, distAttMax, ptPar, degSub, pos);
    }
    
    /**
     * Constructeur de recopie
     * @param m instance de la classe Mage
     */

    public Mage(Mage m) {
        super(m.getNom(), m.getPtVie(), m.getPtMana(), m.getPourcentageAtt(), m.getPourcentagePar(), m.getPourcentageMag(), m.getPourcentageResistMag(), m.getDegAtt(), m.getDegMag(), m.getDistAttMax(), m.getPtPar(), m.getDegSub(), m.getPos());
    }

    /**
     * Constructeur vide
     */

    public Mage() {
        
    }
    
    /**
     * Méthode de combat magique
     * @param c créature avec laquelle le protagoniste va se battre
     */
   
    @Override
    public void combattre(Creature c){
        
        Random aleatoire = new Random();
        int rand = aleatoire.nextInt(100);
        
        if(this.getPos().distance(c.getPos()) >= 1 && this.getDistAttMax() > this.getPos().distance(c.getPos())){
            //combat magique
            this.setPtMana(this.getPtMana()-1);
            if(rand <= this.getPourcentageMag()){
               c.setDegSub(this.getDegMag());
            }
        }
        System.out.println("Fin de combattre");
    }

    @Override
    public void deplacer(World monde) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
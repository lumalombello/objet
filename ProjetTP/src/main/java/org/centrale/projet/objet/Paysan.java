/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 *
 * @author Luma et Luz
 */
public class Paysan extends Personnage{

    /**
     * Constructeur vide
     */
    
    public Paysan() {
    }
    
    /**
     * Constructeur de recopie
     * @param p instance de la clase Paysan
     */
    
    public Paysan(Paysan p) {
        super(p.getNom(), p.getPtVie(), p.getPtMana(), p.getPourcentageAtt(), p.getPourcentagePar(), p.getPourcentageMag(), p.getPourcentageResistMag(), p.getDegAtt(), p.getDegMag(), p.getDistAttMax(), p.getPtPar(), p.getDegSub(), p.getPos());
    }
    
    /**
     * Constructeur de la classe Paysan
     * @param nom                   nom du paysan
     * @param ptVie                 points de vie
     * @param ptMana                points de mana 
     * @param pourcentageAtt        pourcentage d'attaque
     * @param pourcentagePar        pourcentage de parade
     * @param pourcentageMag        pourcentage de magie
     * @param pourcentageResistMag  pourcentage de résistance magique
     * @param degAtt                dégât d'ataque
     * @param degMag                dégât de magie
     * @param distAttMax            distance d'attaque maxime
     * @param pos                   position
     * @param ptPar                 points de parade du défenseur
     * @param degSub                dégâts subis
     */
    
    public Paysan(String nom, int ptVie, int ptMana, int pourcentageAtt, int pourcentagePar, int pourcentageMag, int pourcentageResistMag, int degAtt, int degMag, int distAttMax, int ptPar, int degSub, Point2D pos) {
        super(nom, ptVie, ptMana, pourcentageAtt, pourcentagePar, pourcentageMag, pourcentageResistMag, degAtt, degMag, distAttMax, ptPar, degSub, pos);
    }

    @Override
    public void deplacer(World monde) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

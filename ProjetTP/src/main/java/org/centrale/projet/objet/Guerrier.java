/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

import java.util.Random;

/**
 *
 * @author Luma et Luz
 */

public class Guerrier extends Personnage implements Combattant{
    
    /**
     * Constructeur de classe Guerrier
     * @param nom                       nom du guerrier
     * @param ptVie                     points de vie
     * @param ptMana                    points de mana
     * @param pourcentageAtt            pourcentage d'attaque
     * @param pourcentagePar            pourcentage de parade
     * @param pourcentageMag            pourcentage de magie
     * @param pourcentageResistMag      pourcentage de résistance magique
     * @param degAtt                    dégât d'ataque
     * @param degMag                    dégât de magie
     * @param distAttMax                distance d'attaque maxime
     * @param pos                       position
     * @param ptPar                     points de parade du défenseur
     * @param degSub                    dégâts subis
     */

    public Guerrier(String nom, int ptVie, int ptMana, int pourcentageAtt, int pourcentagePar, int pourcentageMag, int pourcentageResistMag, int degAtt, int degMag, int distAttMax, int ptPar, int degSub, Point2D pos) {
        super(nom, ptVie, ptMana, pourcentageAtt, pourcentagePar, pourcentageMag, pourcentageResistMag, degAtt, degMag, distAttMax, ptPar, degSub, pos);
    }

    /**
     * Constructeur de recopie
     * @param g instance de la classe guerrier
     */

    public Guerrier(Guerrier g) {
        super(g.getNom(), g.getPtVie(), g.getPtMana(), g.getPourcentageAtt(), g.getPourcentagePar(), g.getPourcentageMag(), g.getPourcentageResistMag(), g.getDegAtt(), g.getDegMag(), g.getDistAttMax(), g.getPtPar(), g.getDegSub(), g.getPos());
    }

    /**
     * Constructeur vide
     */

    public Guerrier() {
        
    }
    
    /**
     * Méthode combat corps à corps
     * @param c créature avec laquelle le protagoniste va se battre
     */
    
    @Override
    public void combattre(Creature c){
        
        Random aleatoire = new Random();
        int rand = aleatoire.nextInt(100);
        
        if(this.getPos().distance(c.getPos()) == 1){   
            //combat contact
            if(rand <= this.getPourcentagePar()){
                System.out.println("Victory!");
                if(rand > c.getPourcentagePar()){
                    c.setDegSub(this.getDegAtt());
                }
                else{
                    c.setDegSub(this.getDegAtt()- c.getPtPar()); 
                }
            }
            System.out.println("Defeat!");
        }
        else{
            System.out.println("Pas Possible de Combattre Corps a Corps!");
        }
        System.out.println("Fin de combattre");
    }

    @Override
    public void deplacer(World monde) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
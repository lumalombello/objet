/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 *
 * @author Luma et Luz
 */
public abstract class ElementDeJeu {
    
    private Point2D pos;
    
    public ElementDeJeu(){
        
    }
/**
 * Constructeur de recopie
 * @param pos position de l'élément 
 */
    public ElementDeJeu(Point2D pos) {
        this.pos = new Point2D(pos);
    }
    /**
     * COnstructeur de la clasee ElementDeJeu
     * @param e  instance de la classe ElementDeJeu
     */
    public ElementDeJeu(ElementDeJeu e) {
        this.pos = new Point2D(e.getPos());
    }

    public Point2D getPos() {
        return pos;
    }

    public void setPos(Point2D pos) {
        this.pos = pos;
    }
    
    
    
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 * @author Luma et Luz
 */
public class Lapin extends Monstre{

    /**
     * Constructeur vide
     */
    
    public Lapin() {
        
    }
    
    /**
     * Constructeur de la classe Lapin
     * @param nom            	nom
     * @param ptVie             points de vie
     * @param pourcentageAtt    pourcentage d'attaque
     * @param pourcentagePar    pourcentage de parade
     * @param degAtt            dégât d'ataque
     * @param pos               position
     * @param ptPar             points de parade du défenseur
     * @param degSub            dégâts subis
     */

    public Lapin(String nom, int ptVie, int pourcentageAtt, int pourcentagePar, int degAtt, int ptPar, int degSub, Point2D pos) {
        super(nom, ptVie, pourcentageAtt, pourcentagePar, degAtt, ptPar, degSub, pos);
    }

    /**
     * Constructeur de recopie
     * @param l  instance de la classe Lapin
     */

    public Lapin(Lapin l) {
        super(l.getNom(), l.getPtVie(), l.getPourcentageAtt(), l.getPourcentagePar(), l.getDegAtt(), l.getPtPar(), l.getDegSub(), l.getPos());
    }
    
    /**
     * Autres Methodes
     */

    @Override
    public void affiche(){
        super.affiche();
        System.out.println();
    }

    @Override
    public void deplacer(World monde) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
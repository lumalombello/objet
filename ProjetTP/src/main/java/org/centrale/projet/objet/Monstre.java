	/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 *
 * @author Luma et Luz
 */
public abstract class Monstre extends Creature{
    
    /**
     * Constructeur vide
     */

    public Monstre() {
        super();
    }
    
    /**
     * Constructeur de la classe Monstre
     * @param nom   	        nom 
     * @param ptVie             points de vie 
     * @param pourcentageAtt    pourcentage d'attaque
     * @param pourcentagePar    pourcentage de parade
     * @param degAtt            dégât d'ataque
     * @param pos               position
     * @param ptPar             points de parade du défenseur
     * @param degSub            dégâts subis
     */
    
    public Monstre(String nom, int ptVie, int pourcentageAtt, int pourcentagePar, int degAtt, int ptPar, int degSub, Point2D pos) {
        super(nom, ptVie, pourcentageAtt, pourcentagePar, degAtt, degSub, ptPar, pos);
    }
    
    /**
     * Constructeur de recopie
     * @param m instance de la classe Monstre
     */
    
    public Monstre(Monstre m) {
        super(m.getNom(), m.getPtVie(), m.getPourcentageAtt(), m.getPourcentagePar(), m.getDegAtt(), m.getDegSub(), m.getPtPar(), m.getPos());
    }
}

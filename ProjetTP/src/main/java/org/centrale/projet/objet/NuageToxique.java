/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

import java.util.Random;

/**
 *
 * @author Luma et Luz
 */
public class NuageToxique extends Objet implements Deplacable, Combattant{
    
    private int nivToxicite;
    
    public NuageToxique() {
        
    }

     public NuageToxique(int nivToxicite, Point2D pos) {
        super(pos);
        this.nivToxicite = nivToxicite;
       
    }
    
    /**
     * COnstructeur de recopie
     * @param n instance de la classe NuageToxique
     */
    public NuageToxique(NuageToxique n) {
        super(n.getPos());
        this.nivToxicite = n.getNivToxicite();
    }

    public int getNivToxicite() {
        return nivToxicite;
    }

    public void setNivToxicite(int nivToxicite) {
        this.nivToxicite = nivToxicite;
    }

 
    
    /**
     * Méthode qui permet au nuage de se déplacer
     */
    @Override
    public void deplacer(World monde){
        
        int coordX=0;
        int coordY=0;
        int valeur=2;
        
        while (valeur ==2 || valeur == 3 || valeur == 4 || valeur == 5){
            Random generateurAleatoire = new Random();
            int direction = generateurAleatoire.nextInt(8);

            switch(direction){
                case 0:
                   coordX=getPos().getX();
                   coordY=getPos().getY()+4;
                   break;
                case 1:
                   coordX=getPos().getX()+4;
                   coordY=getPos().getY()+4;
                   break;
                case 2: 
                   coordX=getPos().getX()+4;
                   coordY=getPos().getY();
                   break;
                case 3: 
                   coordX=getPos().getX()+4;
                   coordY=getPos().getY()-4;
                   break;
                case 4: 
                   coordX=getPos().getX();
                   coordY=getPos().getY()-4;
                   break;
                case 5:
                   coordX=getPos().getX()-4;
                   coordY=getPos().getY()-4;
                   break;
                case 6: 
                   coordX=getPos().getX()-4;
                   coordY=getPos().getY();
                   break;
                case 7: 
                   coordX=getPos().getX()-4;
                   coordY=getPos().getY()+4;
                   break;
            }
         valeur = monde.matriceWorld[coordX][coordY];
        }
         
        if(valeur == -1 || valeur==0){
            getPos().setX(coordX);
            getPos().setY(coordY);
            monde.matriceWorld[coordX][coordY]=5;
        }
        if(valeur==1){
            Point2D point = new Point2D();
            point.setX(coordX);
            point.setY(coordY);
            for(int i = 0; i < monde.elements.size(); i++){
                if(monde.elements.get(i).getPos().equals(point)){
                    Creature crea = (Creature)monde.elements.get(i);
                    this.combattre(crea);
                    monde.elements.remove(this);
                    break;
                }
                
            }
        }
    }
        
    
    /**
     * Méthode qui permet au nuage de attaquer un créature
     * @param c créature qui va être attaquée
     */
    @Override
    public void combattre(Creature c){
        Random aleatoire = new Random();
          int rand = aleatoire.nextInt(2);
        
          if (rand==1){
              c.setPtVie(c.getPtVie()-(nivToxicite/3));
          }else{
             c.setPtVie(c.getPtVie()-(nivToxicite/2)); 
          }
    }     
} 
    


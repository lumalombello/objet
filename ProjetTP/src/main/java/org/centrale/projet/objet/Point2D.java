/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 *
 * @author Luma et Luz
 */
public class Point2D {

    /*
     * Attributs
     */
    
    private int x;
    private int y;

    /**
     * Constructeur vide
     */
    
    public Point2D() {
    }

    /**
     * Constructeur de la classe Point2D
     * @param x Coordonné en X
     * @param y Coordonné en Y
     */
    
    public Point2D(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    /**
     * Constructeur de recopie
     * @param p instance de la classe Point2D
     */
    
    public Point2D(Point2D p) {
        this.x = p.getX();
        this.y = p.getY();
    }
    
    /*
     * Getters et Setters
     */
    
    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
   
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    
    /*
     * Autres Methodes
     */
    
    public void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }
    
    /**
     * Methode pour translate le point
     * @param dx Coordonné en X
     * @param dy Coordonné en Y
     */
    
    public void translate(int dx, int dy){
        this.x += dx;
        this.y += dy;
    }
    
    public void affiche(){
        System.out.println("[" + this.getX() + ";" + this.getY() + "]" );
        System.out.println();
    }
    
    /**
     * Méthode pour comparer si un point est égal à un autre
     * @param point point à comparer
     * @return 
     */
    
    public boolean equals(Point2D point){
        return (this.x == point.getX() && this.y == point.getY());
    }
    
    /**
     * Méthode de mesure de la distance entre deux points
     * @param p deuxème point
     * @return 
     */
    
    public float distance(Point2D p){
        float d;
        double a, b;
        a = Math.pow((double)(this.x - p.getX()), 2);
        b = Math.pow((double)(this.y - p.getY()), 2);
        d = (float) Math.sqrt(a+b);
        return d;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

import java.util.Random;
import java.util.Scanner;
import java.util.ArrayList;

/**
 *
 * @author Luma et Luz
 */
public abstract class Personnage extends Creature{
   
    /**
     * Attributs
     */

    private int ptMana;
    private int pourcentageMag;
    private int pourcentageResistMag;
    private int degMag;
    private int distAttMax;
    private int degSub;
    private ArrayList<Nourriture> nourritures;
    
     /**
     * Constructeur de la classe personnage
     * @param nom                   nom du personnage
     * @param ptVie                 points de vie
     * @param ptMana                points de mana 
     * @param pourcentageAtt        pourcentage d'attaque
     * @param pourcentagePar        pourcentage de parade
     * @param pourcentageMag        pourcentage de magie
     * @param pourcentageResistMag  pourcentage de résistance magique
     * @param degAtt                dégât d'ataque
     * @param degMag                dégât de magie
     * @param distAttMax            distance d'attaque maxime
     * @param pos                   position
     * @param ptPar                 points de parade du défenseur
     * @param degSub                dégâts subis
     */
    
    public Personnage(String nom, int ptVie, int ptMana, int pourcentageAtt, int pourcentagePar, int pourcentageMag, int pourcentageResistMag, int degAtt, int degMag, int distAttMax, int ptPar, int degSub, Point2D pos) {
        super(nom, ptVie, pourcentageAtt, pourcentagePar, degAtt, degSub, ptPar, pos);
        this.ptMana = ptMana;
        this.pourcentageMag = pourcentageMag;
        this.pourcentageResistMag = pourcentageResistMag;
        this.degMag = degMag;
        this.distAttMax = distAttMax;
        this.nourritures = new ArrayList();
    }
    
    /**
     * Constructeur de recopie
     * @param p instance de la classe personnage
     */
    
    public Personnage(Personnage p) {
        super(p.getNom(), p.getPtVie(), p.getPourcentageAtt(), p.getPourcentagePar(), p.getDegAtt(), p.getDegSub(), p.getPtPar(), p.getPos());
        this.ptMana = p.getPtMana();
        this.pourcentageMag = p.getPourcentageMag();
        this.pourcentageResistMag = p.getPourcentageResistMag();
        this.degMag = p.getDegMag();
        this.distAttMax = p.getDistAttMax();
     //   this.nourritures = new ArrayList();
    }
    
    /**
     * Constructeur vide
     */
    
    public Personnage() {
        super();
    }
    
    //Getters et Setters
    public void setPtMana(int ptMana) {
        this.ptMana = ptMana;
    }
    
    public int getPtMana() {
        return ptMana;
    }
    
    public void setPourcentageMag(int pourcentageMag) {
        this.pourcentageMag = pourcentageMag;
    }
    
    public int getPourcentageMag() {
        return pourcentageMag;
    }
    
    public void setPourcentageResistMag(int pourcentageResistMag) {
        this.pourcentageResistMag = pourcentageResistMag;
    }
    
    public int getPourcentageResistMag() {
        return pourcentageResistMag;
    }
    
    public void setDegMag(int degMag) {
        this.degMag = degMag;
    }
    
    public int getDegMag() {
        return degMag;
    }
    
    public void setDistAttMax(int distAttMax) {
        this.distAttMax = distAttMax;
    }

    public void setNourritures(ArrayList<Nourriture> nourritures) {
        this.nourritures = nourritures;
    }
    
    public int getDistAttMax() {
        return distAttMax;
    }

    public ArrayList<Nourriture> getNourritures() {
        return nourritures;
    }
    
   /**
    * Méthode qui permet l'affichage d'un personnage
    */
    @Override
    public void affiche(){
        super.affiche();
        System.out.println("PtMana: " + ptMana );  
        System.out.println("PourcentageMag: " + pourcentageMag ); 
        System.out.println("PourcentageResistMag: " +  pourcentageResistMag ); 
        System.out.println("DegMag: " + degMag );
        System.out.println("DistAttMax: " + distAttMax );
        System.out.println();
    }
    
     /**
     * Méthode qui permet à une Personnage de prendre une potion de mana
     * @param m potion de mana
     */
    
    public void prendrePotMana(Mana m){
  
            this.setPtMana(this.getPtMana()+ m.getQtePointsAug());
    
    }
    
    /**
     * Méthode qui permet à une Personnage de prendre une nourriture
     * @param n nourriture
     */
    public void prendreNourriture(Nourriture n){
            nourritures.add(n); 
            nourritures.get(nourritures.size()-1).affecter(this);    
    }
    /**
     * Méthode qui met à jour les tours restants du nourriture dans le jeu et la supprime au cas où les tours sont déjà terminés
     */
    public void mettreAjour(){
        for(Nourriture n : nourritures){
            n.setNbTours(n.getNbTours()-1);
            if(n.getNbTours()==0 ){
                n.cessAffecter(this);
                nourritures.remove(n);
            }
        }
    }
    
}
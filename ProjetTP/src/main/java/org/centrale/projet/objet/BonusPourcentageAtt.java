/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 *
 * @author Luma et Luz
 */
public class BonusPourcentageAtt extends Nourriture {
    
    public BonusPourcentageAtt() {
        
    }
    /**
     * Constructeur de la clase BonusPourcentageAtt
     * @param points points qui augmente le bonus à une certaine caractéristique de personnage
     * @param nbTours nombre de tours qu'un bonus est valable
     * @param pos position du bonus
     */
    public BonusPourcentageAtt(int points, int nbTours, Point2D pos) {
        super(points,nbTours, pos);
    }
    
    /**
     * Constructeur de recopie 
     * @param b  instance de la classe BonusPourcentageAtt
     */
    public BonusPourcentageAtt(BonusPourcentageAtt b) {
        super(b.getPoints(), b.getNbTours(), b.getPos());   
        
    }
    
      /**
     * Cette méthode augmente le pourcentage d'attaque d'une créature d'un certain nombre de points
     * @param c  instance de la classe Creature
     */
    
    @Override
    public void affecter(Creature c){
        if(getNbTours() != 0){
            c.setPourcentageAtt(c.getPourcentageAtt()+ getPoints()); 
            System.out.println("Augmentation de " +getPoints() + " points de pourcentage attaque");
        }  
        
    }
    
     /**
     * Cette méthode inverse l'effet de la méthode affecter()
     * @param c  instance de la classe Creature
     */  
    @Override
    public void cessAffecter(Creature c){
        c.setPourcentageAtt(c.getPourcentageAtt()- getPoints());  
    }
        
      
    
}

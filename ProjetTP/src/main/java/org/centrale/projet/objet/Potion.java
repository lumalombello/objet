/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 *
 * @author Luma et Luz
 */
public class Potion extends Objet {
    
    /*
     * Attributs
     */
    
    private int qtePointsAug;
    private boolean consommé;

    /**
     * Constructeur vide
     */
    
    public Potion() {
        
    }

    /**
     * Constructeur de la classe potion
     * @param qtePointsAug quantité de points à agumenter
     * @param pos           position
     */
    
    public Potion(int qtePointsAug, Point2D pos) {
        super(pos);
        this.qtePointsAug = qtePointsAug;
    }

    /**
     * Constructeur de recopie
     * @param p instance de la clase Potion
     */
    
    public Potion(Potion p) {
        super(p.getPos());
        this.qtePointsAug = p.getQtePointsAug();
    }
    
    /*
     * Getters et Setters
     */
    
    public int getQtePointsAug() {
        return qtePointsAug;
    }

    public void setQtePointsAugAug(int qtePointsAug) {
        this.qtePointsAug = qtePointsAug;
    }
    
}

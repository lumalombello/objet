/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.centrale.projet.objet;

import java.util.Random;
import java.util.Scanner;

/**
 * @author Luma et Luz
 */

public class Archer extends Personnage implements Combattant{
    
    /**
     * Attributs
     */
    private int nbFleches;
    
    /**
     * Constructeur de classe Archer
     * @param nom                   nom de l'archer
     * @param ptVie                 points de vie 
     * @param ptMana                points de mana 
     * @param pourcentageAtt        pourcentage d'attaque
     * @param pourcentagePar        pourcentage de parade
     * @param pourcentageMag        pourcentage de magie
     * @param pourcentageResistMag  pourcentage de résistance magique
     * @param degAtt                dégât d'ataque
     * @param degMag                dégât de magie
     * @param distAttMax            distance d'attaque maxime
     * @param pos                   position
     * @param nbFleches             nombre de fleches
     * @param ptPar                 points de parade du défenseur
     * @param degSub                dégâts subis
     */
    
    public Archer(String nom, int ptVie, int ptMana, int pourcentageAtt, int pourcentagePar, int pourcentageMag, int pourcentageResistMag, 
            int degAtt, int degMag, int distAttMax, int ptPar, int degSub, int nbFleches, Point2D pos) {
        super(nom, ptVie, ptMana, pourcentageAtt,pourcentagePar, pourcentageMag, pourcentageResistMag,degAtt,degMag,distAttMax, ptPar, degSub,pos);
        this.nbFleches = nbFleches;
    }
    
    /**
     * Constructeur de recopie
     * @param a instance de la classe Archer
     */
    
    public Archer(Archer a) {
        super(a.getNom(), a.getPtVie(), a.getPtMana(), a.getPourcentageAtt(),a.getPourcentagePar(), a.getPourcentageMag(), a.getPourcentageResistMag(),a.getDegAtt(),a.getDegMag(),a.getDistAttMax(), a.getPtPar(), a.getDegSub(),a.getPos());
        this.nbFleches = a.nbFleches;
    }
    
    /**
     * Constructeur vide
     */
    
    public Archer() {
        super();
        this.nbFleches = 0;
    }
     
    /**
     * Setters et Getters
     * @param nbFleches
     */
   
    public void setNbFleches(int nbFleches) {
        this.nbFleches = nbFleches;
    }
    
    public int getNbFleches() {
        return nbFleches;
    }
    
    /**
     * Autres methodes
     */
   
    @Override
    public void affiche (){
        super.affiche();
        System.out.println("NbFleches" + nbFleches);
        System.out.println();
    }
    
    /**
     * Méthode de combat à distance
     * @param c  créature avec laquelle le protagoniste va se battre
     */
    
    @Override
    public void combattre(Creature c){
        Random aleatoire = new Random();
        int rand = aleatoire.nextInt(100); 
        if(this.getPos().distance(c.getPos()) > 1 && this.getDistAttMax() > this.getPos().distance(c.getPos())){
            //combat distance
            this.setNbFleches(this.getNbFleches()-1);
            if(rand <= this.getPourcentageAtt()){
               c.setDegSub(this.getDegAtt());
            }
        }
        System.out.println("Fin de combattre");
    }  

    @Override
    public void deplacer(World monde) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public int stackErrorFleches(int nb){
        if(nb == 0){
            return nb;
        }
        else{
            stackErrorFleches(nb);
        }
        return nb;
    }
 
    
}
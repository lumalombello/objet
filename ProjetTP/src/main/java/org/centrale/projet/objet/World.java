/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 *
 * @author Luma et Luz
 */

public class World {
    
    public int dimension; //= matrice size
    public int qteAleaCreat;
    public int matriceWorld[][] ;

    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public int getQteAleaCreat() {
        return qteAleaCreat;
    }

    public void setQteAleaCreat(int qteAleaCreat) {
        this.qteAleaCreat = qteAleaCreat;
    }

    public int[][] getMatriceWorld() {
        return matriceWorld;
    }

    public void setMatriceWorld(int[][] matriceWorld) {
        this.matriceWorld = matriceWorld;
    }
    public ArrayList<ElementDeJeu> elements;
    public Joueur j;
    
    /**
     * Constructeur
     */
    
    public World() {
        this.dimension = 50;
        this.matriceWorld = new int[dimension][dimension];
        this.qteAleaCreat = 50;
        elements = new ArrayList<>();
    }
   
    
    /**
     *Methode qui reinitialise le monde 
     */
    
    public void creationJoueur(){
        j = new Joueur();
        j.choixPersonnage();
        j.getPerso().setPos(this.distMin3());
        this.matriceWorld[j.getPerso().getPos().getX()][j.getPerso().getPos().getY()] = 6;

    }
    /**
     * Methode qui gére les tours de jeu
     * @throws IOException 
     */
    public void tourDeJeu() throws IOException{
        this.afficheWorld();
        j.action(this);
        j.getPerso().mettreAjour();
 
    }
    //acaa
    public int validerPosition(Point2D point){
       int x = point.getX();
       int y = point.getY();
       int value = this.matriceWorld[x][y];
       return value;     
    }
    //
    /** Methode newWorld () qui ne reçoit pas de parametres et qui n'a pas de retour
     * 
     *  Elle doit generer une matrice de monde initialise avec 0 et qu'apres genere 
     *  une quantite aleatoire pour chaque type de protagoniste qu'est instancie avec
     *  un nom et un point et les insere sur la matrice du monde
     */
    
    public void creeMondeAlea(){
        
        
        for(int i = 0; i < dimension; i++){
            for(int j = 0; j < dimension; j++){
                matriceWorld[i][j] = 0;
            }
        }
        
        
        Random aleatoire = new Random();
        String nom;
        
        int quantiteArcher = aleatoire.nextInt(qteAleaCreat);
        for(int i = 0; i < quantiteArcher; i++){  
            nom = "archer" + Integer.toString(i);
            Archer a = new Archer();
            a.setNom(nom);
            elements.add(a);   
            a.setPos(this.distMin3()); 
        }
        
        int quantiteGuerrier = aleatoire.nextInt(qteAleaCreat);
        for(int i = 0; i < quantiteGuerrier; i++){
            nom = "guerrier" + Integer.toString(i);
            Guerrier g = new Guerrier();
            g.setNom(nom);
            elements.add(g);
            g.setPos(this.distMin3()); 
        }
        
        int quantiteMage = aleatoire.nextInt(qteAleaCreat);
        for(int i = 0; i < quantiteMage; i++){
            nom = "mage" + Integer.toString(i);
            Mage m = new Mage();
            m.setNom(nom);
            elements.add(m);
            m.setPos(this.distMin3()); 
        }
        
        int quantitePaysan = aleatoire.nextInt(qteAleaCreat);
        for(int i = 0; i < quantitePaysan; i++){
            nom = "paysan" + Integer.toString(i);
            Paysan p = new Paysan();
            p.setNom(nom);
            elements.add(p);
            p.setPos(this.distMin3()); 
        }
        
        int quantiteLoup = aleatoire.nextInt(qteAleaCreat);
        for(int i = 0; i < quantiteLoup; i++){
            nom = "loup" + Integer.toString(i);
            Loup lo = new Loup();
            lo.setNom(nom);
            elements.add(lo);
            lo.setPos(this.distMin3()); 
        }
        
        int quantiteLapin = aleatoire.nextInt(qteAleaCreat);
        for(int i = 0; i < quantiteLapin; i++){
            nom = "lapin" + Integer.toString(i);
            Lapin la = new Lapin();
            la.setNom(nom);
            elements.add(la);
            la.setPos(this.distMin3()); 
        }
        
        int quantiteSoin = aleatoire.nextInt(qteAleaCreat);
        int value=2; 
        for(int i=0; i<quantiteSoin; i++){
            Soin so = new Soin();
            so.setQtePointsAugAug(10);
            elements.add(so);
            so.setPos(this.positionLibre(value));
        }
        
        int quantiteMana = aleatoire.nextInt(qteAleaCreat);
        value=3; 
        for(int i=0; i<quantiteMana; i++){
            Mana ma = new Mana();
            ma.setQtePointsAugAug(10);
            elements.add(ma);
            ma.setPos(this.positionLibre(value));
        }
        
        int quantiteBonusPorcAtt = aleatoire.nextInt(qteAleaCreat);
        value=4; 
        for(int i=0; i<quantiteBonusPorcAtt; i++){
            BonusPourcentageAtt bo = new BonusPourcentageAtt();
            bo.setNbTours(5);
            bo.setPoints(12);
            elements.add(bo);
            bo.setPos(this.positionLibre(value));
        }
       
        int quantiteMalusPorcAtt = aleatoire.nextInt(qteAleaCreat);
        value=4;
        for(int i=0; i<quantiteMalusPorcAtt; i++){
            MalusPourcentageAtt mal = new MalusPourcentageAtt();
            mal.setNbTours(5);
            mal.setPoints(13);
            elements.add(mal);
            mal.setPos(this.positionLibre(value));
        }
        value=5;
        NuageToxique nuage = new NuageToxique();
        nuage.setNivToxicite(20);
        elements.add(nuage);
        nuage.setPos(this.positionLibre(value));
        
             
    }    
    
   
   /**
    * Méthode qui génère un nombre aléatoire jusqu'à ce qu'elle trouve une case libre
    * @param value
    * @return elle renvoit un point avec les coordones de la cas libre
    */
   public Point2D positionLibre(int value){
           Random generateurAleatoire = new Random();
           int  rndX = generateurAleatoire.nextInt(dimension);
           int rndY = generateurAleatoire.nextInt(dimension);
           
           while (matriceWorld[rndX][rndY] != 0 && matriceWorld[rndX][rndY] != -1){
            rndX = generateurAleatoire.nextInt(dimension);
            rndY = generateurAleatoire.nextInt(dimension);
        }
        
        matriceWorld[rndX][rndY]=value;
        Point2D point = new Point2D(rndX,rndY);
        return point;
       
   } 
 /** *  Methode distMin3 () qui ne reçoit pas de parametres et qui retourne un Point2D
     * 
     *  Elle doit generer un point aleatoire, vérifier si cette position est libre et 
     *  vérifier des conditions des bords de la matrice.Après, allocation de -1 pour 
        les places qui sont a cote a moins de 3 de distance du point genere. et retour 
        du point.
     * @return Point2D
     */
    public Point2D distMin3 (){
        Random aleatoire = new Random();

        int xAlea = aleatoire.nextInt(dimension-1);
        int yAlea = aleatoire.nextInt(dimension-1);

        while (matriceWorld[xAlea][yAlea] != 0){
            xAlea = aleatoire.nextInt(dimension);
            yAlea = aleatoire.nextInt(dimension);
        }

        int dimMinX = -2;
        int dimMinY = -2;
        int dimMaxX = 3;
        int dimMaxY = 3;
        int dim1, dim2;
        
        if (xAlea == 0){
            dimMinX = 0;
        }
        else if(xAlea == 1){
            dimMinX = -1;
        }
        else if(xAlea == dimension-1){
            dimMaxX = 1;
        }
        else  if(xAlea == dimension-2){
            dimMaxX = 2;
        }
        
        if (yAlea == 0){
            dimMinY = 0;
        }
        else if(yAlea == 1){
            dimMinY = -1;
        }
        else if(yAlea == dimension-1){
            dimMaxY = 1;
        }
        else  if(yAlea == dimension-2){
            dimMaxY = 2;
        }

        for(dim1 = dimMinX; dim1 < dimMaxX; dim1++){
           for( dim2 = dimMinY; dim2 < dimMaxY; dim2++){
                matriceWorld[xAlea+dim1][yAlea+dim2] = -1;
            } 
        }

        matriceWorld[xAlea][yAlea] = 1;

        Point2D point = new Point2D(xAlea, yAlea);

        return point;
    }
   
    /**
     * Methode qui affiche le monde
     */
    
    public void afficheWorld(){
        System.out.println("Affichage World");
        int valeur;
        for(int i = 0; i < this.dimension; i++){
            for(int j = 0; j < this.dimension; j++){
                valeur = this.matriceWorld[i][j];
                switch(valeur){
                    case -1:
                    case 0:
                        System.out.print("00 ");
                        break;
                    case 1:
                        Point2D point = new Point2D(i,j);
                        this.rechercherClasseCreature(point);
                        break;
                    case 2:
                        System.out.print("So ");
                        break;
                    case 3:
                        System.out.print("Mn ");
                        break;
                    case 4:
                        System.out.print("No ");
                        break;
                    case 5:
                        System.out.print("To ");
                        break;
                    case 6:
                        System.out.print("Jo ");
                        break;
                }   
            }
              System.out.println(" ");        
        } 
    } 
    /**
     * Méthode auxiliaire pour montrer le monde, elle permet de connaître la classe à laquelle appartient 
     * une créature qui se trouve dans un point précis. Pour cela il reçoit comme paramètre un objet de la classe Point2D.
     * @param point 
     */
     public void rechercherClasseCreature (Point2D point){
        String typeClasse;
        for(int i = 0; i < this.elements.size(); i++){
            if(this.elements.get(i).getPos().equals(point)){
               typeClasse = this.elements.get(i).getClass().getSimpleName();
                 switch(typeClasse){
                    case "Archer":
                        System.out.print("Ar ");
                        break;
                    case "Guerrier":
                        System.out.print("Ge ");
                        break;
                    case "Mage":
                        System.out.print("Ma ");
                        break;
                    case "Paysan":
                        System.out.print("Pa ");
                        break;
                    case "Loup":
                        System.out.print("Lo ");
                        break;
                    case "Lapin":
                        System.out.print("La ");
                        break;
                }
                break;
            }
        }
       
    }
    
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

import java.util.Random;

/**
 * @author Luma et Luz
 */
 
public class Loup extends Monstre implements Combattant{

    /**
     * Constructeur vide
     */
     
    public Loup() {
        
    }

    /**
     * Constructeur de la classe Loup
     * @param nom	            nom 
     * @param ptVie             points de vie 
     * @param pourcentageAtt    pourcentage d'attaque
     * @param pourcentagePar    pourcentage de parade
     * @param degAtt            dégât d'ataque
     * @param pos               position
     * @param ptPar             points de parade du défenseur
     * @param degSub            dégâts subis
     */

    public Loup(String nom, int ptVie, int pourcentageAtt, int pourcentagePar, int degAtt, int ptPar, int degSub, Point2D pos) {
        super(nom, ptVie, pourcentageAtt, pourcentagePar, degAtt, ptPar, degSub, pos);
    }

    /**
     * Constructeur de recopie
     * @param l instance de la classe Loup
     */

    public Loup(Loup l) {
        super(l.getNom(), l.getPtVie(), l.getPourcentageAtt(), l.getPourcentagePar(), l.getDegAtt(), l.getPtPar(), l.getDegSub(), l.getPos());
    }
    
    /**
     * Autres Methodes
     * @param c
     */
     
    @Override
    public void combattre(Creature c){
        Random aleatoire = new Random();
        int rand = aleatoire.nextInt(100);
        
        if(this.getPos().distance(c.getPos()) == 1){   
            //combat contact
            if(rand <= this.getPourcentagePar()){
                //attaque reussite
                if(rand > c.getPourcentagePar()){
                    c.setDegSub(this.getDegAtt());
                }
                else{
                    c.setDegSub(this.getDegAtt()- c.getPtPar()); 
                }
            }
        }
        System.out.println("Fin de combattre");
    }

    @Override
    public void deplacer(World monde) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
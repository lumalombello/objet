/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 *
 * @author Luma et Luz
 */
public class TestPoint2D {
    
    public static void main(String[] args){
        
        //Creation des points avec les 3 types de constructeurs
       
        Point2D p1 = new Point2D();
        Point2D p2 = new Point2D(4,5);
        Point2D p3 = new Point2D(7,2);
        Point2D p4 = new Point2D(p3);
        
        //Utilisation des methodes setters, getters et affichage
        
        p1.setPosition(0, 0);
        System.out.println("Affichage p1 apres methode setPosition");
        p1.affiche();
        
        //Utilisartion des methodes translate, getters et affichage
        
        p2.translate(-1, 1);
        System.out.println("Affichage p2 apres methode translate");
        p2.affiche();
        
        //Utilisation des methodes setters, getters et affichage
        
        p3.setX(3);
        p3.setY(3);
        
        System.out.println("Affichage p3 en utilisant getX et getY - apres methodes setX et setY");
        System.out.println("[" + p3.getX() + ";" + p3.getY() + "]");
        
        System.out.println("Affichage p4 cree avec Constructeur de Recopie");
        p4.affiche();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 *
 * @author Luz
 */
public  class MalusPourcentageAtt extends Nourriture {
    
  public MalusPourcentageAtt() {

    } 
  
  /**
   * Constructeur de la classe MalusPourcentageAtt
   * @param points points qui réduit le malus à une certaine caractéristique de personnage
   * @param nbTours nombre de tours qu'un malus est valable
   * @param pos position du malus
   */
    public MalusPourcentageAtt(int points, int nbTours, Point2D pos) {
        super(points,nbTours,pos);
    }
    
     /**
     * Constructeur de recopie
     * @param m instance de la clase MalusPourcentageAtt
     */
    public MalusPourcentageAtt(MalusPourcentageAtt m) {
        super(m.getPoints(), m.getNbTours(),  m.getPos());   
       
    }
    
    /**
     * Cette méthode réduit le pourcentage d'attaque d'une créature d'un certain nombre de points
     * @param c creature à affecter
     */
    @Override
    public void affecter(Creature c) {
        if(getNbTours() != 0){ //no se si sea necesario
            c.setPourcentageAtt(c.getPourcentageAtt()- getPoints());
            System.out.println("Reduction de " +getPoints() + " points de pourcentage attaque");
        } 
    }
     
      /**
     * Cette méthode inverse l'effet de la méthode affecter()
     * @param c creature à cesser d'affecter
     */
    @Override
     public void cessAffecter(Creature c){
        c.setPourcentageAtt(c.getPourcentageAtt()+ getPoints());  
    }
    
    
}

    
   

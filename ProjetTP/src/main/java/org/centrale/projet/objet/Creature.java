/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;
import java.util.Random;

/**
 * @author Luma et Luz
 */

public abstract class Creature extends ElementDeJeu implements Deplacable{
    
    /**
     * Attributs
     */
     
    private String nom;
    private int ptVie;
    private int pourcentageAtt;
    private int pourcentagePar;
    private int degAtt;
    private int ptPar;
    private int degSub;
    
    /**
     * Constructeur vide
     */
     
    public  Creature() {
    }
    
    /**
     * Constructeur de recopie
     * @param c   instance de la classe Creature
     */
    
    public Creature(Creature c) {
        super( c.getPos());
        this.nom = c.getNom();
        this.ptVie = c.getPtVie();
        this.pourcentageAtt = c.getPourcentageAtt();
        this.pourcentagePar = c.getPourcentagePar();
        this.degAtt = c.getDegAtt();
        this.degSub = c.getDegSub();
        this.ptPar = c.getPtPar();
       
    }

    /**
     * @param nom             	nom
     * @param ptVie             points de vie 
     * @param pourcentageAtt    pourcentage d'attaque
     * @param pourcentagePar    pourcentage de parade
     * @param degAtt            dégât d'ataque
     * @param pos               position
     * @param degSub            dégâts subis
     * @param ptPar             points de parade du défenseur
     */

    public Creature(String nom, int ptVie, int pourcentageAtt, int pourcentagePar, int degAtt, int degSub, int ptPar, Point2D pos) {
        super(pos);
        this.nom = nom;
        this.ptVie = ptVie;
        this.pourcentageAtt = pourcentageAtt;
        this.pourcentagePar = pourcentagePar;
        this.degAtt = degAtt;
        this.degSub = degSub;
        this.ptPar = ptPar;
    }
    
    /**
     * Getters et Setters
     */
     
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    public int getPtVie() {
        return ptVie;
    }

    public int getPourcentageAtt() {
        return pourcentageAtt;
    }

    public int getPourcentagePar() {
        return pourcentagePar;
    }

    public int getDegAtt() {
        return degAtt;
    }

    public void setPtVie(int ptVie) {
        this.ptVie = ptVie;
    }

    public void setPourcentageAtt(int pourcentageAtt) {
        this.pourcentageAtt = pourcentageAtt;
    }

    public void setPourcentagePar(int pourcentagePar) {
        this.pourcentagePar = pourcentagePar;
    }

    public void setDegAtt(int degAtt) {
        this.degAtt = degAtt;
    }

    
    public void setPtPar(int ptPar) {
        this.ptPar = ptPar;
    }

    public int getPtPar() {
        return ptPar;
    }
    public int getDegSub() {
        return degSub;
    }

    public void setDegSub(int degSub) {
        this.degSub = degSub;
    }
     
    /**
     * Méthode qui permet au protagoniste de se déplacer vers une case aléatoire adjacente, 
     * cette méthode ne reçoit pas de paramètres et ne renvoie rien
     */ 
     
    @Override
    public void deplacer(World monde){
        
        int coordX=0;
        int coordY=0;
        int valeur=3;
        
        while (valeur !=0 && valeur != -1){
            Random generateurAleatoire = new Random();
            int direction = generateurAleatoire.nextInt(8);

            switch(direction){
                case 0:
                   coordX=getPos().getX();
                   coordY=getPos().getY()+1;
                   break;
                case 1:
                   coordX=getPos().getX()+1;
                   coordY=getPos().getY()+1;
                   break;
                case 2: 
                   coordX=getPos().getX()+1;
                   coordY=getPos().getY();
                   break;
                case 3: 
                   coordX=getPos().getX()+1;
                   coordY=getPos().getY()-1;
                   break;
                case 4: 
                   coordX=getPos().getX();
                   coordY=getPos().getY()-1;
                   break;
                case 5:
                   coordX=getPos().getX()-1;
                   coordY=getPos().getY()-1;
                   break;
                case 6: 
                   coordX=getPos().getX()-1;
                   coordY=getPos().getY();
                   break;
                case 7: 
                   coordX=getPos().getX()-1;
                   coordY=getPos().getY()+1;
                   break;
            }
         valeur = monde.matriceWorld[coordX][coordY];
        }
        getPos().setX(coordX);
        getPos().setY(coordY);
        monde.matriceWorld[coordX][coordY]=1;
    }
    
    /**
     * Méthode qui montre les attributs d'un protagoniste
     */
    
  
     
    public void affiche(){
        System.out.println("Nom = " + this.getNom());
        System.out.println("PtVie = " + this.getPtVie());
        System.out.println("PourcentageAtt = " + this.getPourcentageAtt());
        System.out.println("PourcentagePar = " + this.getPourcentagePar());
        System.out.println("DegAtt = " + this.getDegAtt());
        System.out.println("Pos = [" + this.getPos().getX() + ";" + this.getPos().getY() + "]");
    }
    
    /**
     * Méthode qui permet à une Creature de prendre une potion de vie 
     * @param s  Potion de vie
     */
    
    public void prendrePotVie(Soin s){
      
            this.setPtVie(this.getPtVie()+ s.getQtePointsAug());
           
    }
    
   
    
}
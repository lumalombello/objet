/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 * @author Luma et Luz
 */

public class Mana extends Potion{

    /**
     * Constructeur vide
     */
    
    public Mana() {
    }
    
    /**
     * Constructeur classe Mana
     * @param qtePointsAug  quantié de points à augmenter
     * @param pos           position
     */
    
    public Mana(int qtePointsAug, Point2D pos) {
        super(qtePointsAug, pos);
    }
    
    /**
     * Constructeur de recopie
     * @param m instance de la classe Mana
     */
    
    public Mana(Mana m) {
        super(m.getQtePointsAug(), m.getPos());
    }
    
}

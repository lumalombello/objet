/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 *
 * @author Luma et Luz
 */
public class Soin extends Potion{
     
    /**
     * Constructeur vide
     */
    
    public Soin() {
    }

    /**
     * Constructeur de la classe Soin
     * @param qtePointsAug  quantité de points à agumenter
     * @param pos           position
     */
    
    public Soin(int qtePointsAug, Point2D pos) {
        super(qtePointsAug, pos);
    }

    /**
     * Constructeur de recopie
     * @param s instance de la clase Soin
     */
    
    public Soin(Soin s) {
        super(s.getQtePointsAug(), s.getPos());
    }
    
    
    
}

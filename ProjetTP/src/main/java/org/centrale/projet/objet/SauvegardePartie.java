/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

import java.io.*;


/**
 *
 * @author Luz
 */
public class SauvegardePartie {
    
    BufferedWriter bufferedWriter;
    String nomFichier;
    
    /**
     * Constructeur de la clase SauvegardePartie
     * @param nomFichier  nom du fichier qui va être enregistrée 
     * @throws IOException exception si il y a un problème lors de l'écriture dans le fichier 
     */
     public SauvegardePartie(String nomFichier) throws IOException {
        this.nomFichier = nomFichier;
        this.bufferedWriter =  new BufferedWriter(new FileWriter(nomFichier));
    }
     
    /**
     * Cette methode  cette méthode permet d'enregistrer les données des éléments du monde dans un fichier.
     * Pour cela il reçoit comme paramètre un objet de la classe World.
     * @param monde 
     */ 
    public void sauvegarderPartie(World monde){
        
        
        try {
            bufferedWriter.write("Dimension " + Integer.toString(monde.dimension));
            bufferedWriter.newLine();
            for(ElementDeJeu e: monde.elements){
                bufferedWriter.write(getTexteSauvegarde(e));
                bufferedWriter.newLine();  
            }
            bufferedWriter.write(monde.j.getClass().getSimpleName()+" " + 
                    getTexteSauvegarde(monde.j.getPerso()));
            
        }catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } 
        catch (IOException ex) {
            ex.printStackTrace();
        }
        finally {
            try {
                if (bufferedWriter != null) {
                    // je force l'écriture dans le fichier
                    bufferedWriter.flush();
                    // puis je le ferme
                    bufferedWriter.close();
                }
            }
            // on attrape l'exception potentielle 
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
        
  }
  /**
   * Cette méthode permet de gérer la sauvegarde des différents éléments du jeu. Il retourne une chaine 
   * de caractères avec les données de un élément. Pour cela il reçoit comme paramètre un objet de la classe ElementDeJeu 
   * @param e element 
   * @return 
   */  
  public String getTexteSauvegarde(ElementDeJeu e){
      String ligne;
    if (e instanceof Potion){
          //Mana,Soin
         ligne = e.getClass().getSimpleName() + " " +Integer.toString(((Potion) e).getQtePointsAug()) + " " +
                 Integer.toString(e.getPos().getX())+ " " +Integer.toString(e.getPos().getY());
       
    }else if(e instanceof NuageToxique ){
        ligne = e.getClass().getSimpleName()+ " " + Integer.toString(((NuageToxique) e).getNivToxicite()) + " " +
                 Integer.toString(e.getPos().getX())+ " " + Integer.toString(e.getPos().getY());
    }else if(e instanceof Nourriture){
        ligne = e.getClass().getSimpleName() + " " +Integer.toString(((Nourriture) e).getPoints()) + " " +Integer.toString(((Nourriture) e).getNbTours())  + " " +
                 Integer.toString(e.getPos().getX())+ " " + Integer.toString(e.getPos().getY());
    }else if(e instanceof Personnage){
        
            String ligneTemp = e.getClass().getSimpleName() + " " + ((Personnage) e).getNom() + " " + Integer.toString(((Personnage) e).getPtVie())+ " " + Integer.toString(((Personnage) e).getPtMana()) + " " +
                        Integer.toString(((Personnage) e).getPourcentageAtt())+ " " + Integer.toString(((Personnage) e).getPourcentagePar())+ " " +
                        Integer.toString(((Personnage) e).getPourcentageMag()) + " " + Integer.toString(((Personnage) e).getPourcentageResistMag())+ " " +
                        Integer.toString(((Personnage) e).getDegAtt())+ " " + Integer.toString(((Personnage) e).getDegMag())+ " "+
                        Integer.toString(((Personnage) e).getDistAttMax()) + " " + Integer.toString(((Personnage) e).getPtPar()) +" " + 
                        Integer.toString(((Personnage) e).getDegSub());
       
       if( e instanceof Archer){
           ligne = ligneTemp + " " +Integer.toString(((Archer) e).getNbFleches()) +" " +  Integer.toString(e.getPos().getX()) + " "+ Integer.toString(e.getPos().getY());
       }else{
           ligne = ligneTemp + " " +  Integer.toString(e.getPos().getX()) + " "+ Integer.toString(e.getPos().getY());;
       }       
       
    }else if(e instanceof Monstre){
        ligne= e.getClass().getSimpleName() + " " +((Monstre) e).getNom() + " "+  Integer.toString(((Monstre) e).getPtVie()) + " " + 
                Integer.toString(((Monstre) e).getPourcentageAtt()) + " " + Integer.toString(((Monstre) e).getPourcentagePar())  + " "+ 
                Integer.toString(((Monstre) e).getDegAtt()) + " " + Integer.toString(((Monstre) e).getPtPar()) + " "+ 
                Integer.toString(((Monstre) e).getDegSub()) + " " + Integer.toString(e.getPos().getX()) + " " +
                Integer.toString(e.getPos().getY());
    }else{
        ligne = "";
    }
      
  
      return ligne;
  }
     
}

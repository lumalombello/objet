/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 *
 * @author Luma et Luz
 */
public abstract class Nourriture extends Objet{
    
    private int points;
    private int nbTours;
    
    
    
     public Nourriture() {
      
    }
    /**
     * Constructeur de la classe Nourriture
     * @param points   points qui modifie la nourriture à une certaine caractéristique de personnage
     * @param nbTours  nombre de tours qu'une nourriture est valable
     * @param pos      position de la nourriture
     */
    public Nourriture(int points, int nbTours, Point2D pos) {
        super(pos);
        this.points = points;
        this.nbTours = nbTours;
    }
    
     /**
     * Constructeur de recopie
     * @param n instance de la classe Nourriture
     */
    public Nourriture(Nourriture n) {
        super(n.getPos());
        this.points = n.getPoints();
        this.nbTours = n.getNbTours();
        
    }

    public int getPoints() {
        return points;
    }

    public int getNbTours() {
        return nbTours;
    }

    public void setNbTours(int nbTours) {
        this.nbTours = nbTours;
    }

    public void setPoints(int points) {
        this.points = points;
    }
    
    public abstract void affecter(Creature c);
    public abstract void cessAffecter(Creature c);
        
    
    
}

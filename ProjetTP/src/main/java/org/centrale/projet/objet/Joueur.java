/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Luma et Luz
 */
public class Joueur {

    private Personnage perso;
    private String typePerso;

    public String getTypePerso() {
        return typePerso;
    }

    public void setTypePerso(String typePerso) {
        this.typePerso = typePerso;
    }

    public Personnage getPerso() {
        return perso;
    }

    public void setPerso(Personnage perso) {
        this.perso = perso;
    }
    
    public Joueur() {
        
    }
    
    public Joueur(ArrayList<String> caracteristiquesPerso, Point2D pos) {
        this.typePerso = caracteristiquesPerso.get(1);
        switch(this.typePerso){
            case "archer":
                perso = new Archer(caracteristiquesPerso.get(2), Integer.parseInt(caracteristiquesPerso.get(3)), +
                        + Integer.parseInt(caracteristiquesPerso.get(4)), Integer.parseInt(caracteristiquesPerso.get(5)), +
                        + Integer.parseInt(caracteristiquesPerso.get(6)), Integer.parseInt(caracteristiquesPerso.get(7)), +
                        + Integer.parseInt(caracteristiquesPerso.get(8)), Integer.parseInt(caracteristiquesPerso.get(9)), +
                        + Integer.parseInt(caracteristiquesPerso.get(10)), Integer.parseInt(caracteristiquesPerso.get(11)), +
                        + Integer.parseInt(caracteristiquesPerso.get(12)), Integer.parseInt(caracteristiquesPerso.get(13)), +
                        + Integer.parseInt(caracteristiquesPerso.get(14)), pos);
                break;
            case "guerrier":
                perso = new Guerrier(caracteristiquesPerso.get(2), Integer.parseInt(caracteristiquesPerso.get(3)), +
                        + Integer.parseInt(caracteristiquesPerso.get(4)), Integer.parseInt(caracteristiquesPerso.get(5)), +
                        + Integer.parseInt(caracteristiquesPerso.get(6)), Integer.parseInt(caracteristiquesPerso.get(7)), +
                        + Integer.parseInt(caracteristiquesPerso.get(8)), Integer.parseInt(caracteristiquesPerso.get(9)), +
                        + Integer.parseInt(caracteristiquesPerso.get(10)), Integer.parseInt(caracteristiquesPerso.get(11)), +
                        + Integer.parseInt(caracteristiquesPerso.get(12)), Integer.parseInt(caracteristiquesPerso.get(13)), pos);
                break;
            case "mage":
                perso = new Mage(caracteristiquesPerso.get(2), Integer.parseInt(caracteristiquesPerso.get(3)), +
                        + Integer.parseInt(caracteristiquesPerso.get(4)), Integer.parseInt(caracteristiquesPerso.get(5)), +
                        + Integer.parseInt(caracteristiquesPerso.get(6)), Integer.parseInt(caracteristiquesPerso.get(7)), +
                        + Integer.parseInt(caracteristiquesPerso.get(8)), Integer.parseInt(caracteristiquesPerso.get(9)), +
                        + Integer.parseInt(caracteristiquesPerso.get(10)), Integer.parseInt(caracteristiquesPerso.get(11)), +
                        + Integer.parseInt(caracteristiquesPerso.get(12)), Integer.parseInt(caracteristiquesPerso.get(13)), pos);
                break;
            case "paysan":
                perso = new Paysan(caracteristiquesPerso.get(2), Integer.parseInt(caracteristiquesPerso.get(3)), +
                        + Integer.parseInt(caracteristiquesPerso.get(4)), Integer.parseInt(caracteristiquesPerso.get(5)), +
                        + Integer.parseInt(caracteristiquesPerso.get(6)), Integer.parseInt(caracteristiquesPerso.get(7)), +
                        + Integer.parseInt(caracteristiquesPerso.get(8)), Integer.parseInt(caracteristiquesPerso.get(9)), +
                        + Integer.parseInt(caracteristiquesPerso.get(10)), Integer.parseInt(caracteristiquesPerso.get(11)), +
                        + Integer.parseInt(caracteristiquesPerso.get(12)), Integer.parseInt(caracteristiquesPerso.get(13)), pos);
                break;
        }
    }
    /**
     * Méthode qui permet au joueur de choisir un personnage
     */
    public void choixPersonnage(){
        Scanner scanIn = new Scanner(System.in);
        
        System.out.println("Taper le type de Personnage avec lequelle tu veux jouer (Archer, Guerrier, Mage ou Paysan):");
        typePerso = scanIn.nextLine();
        
        System.out.println("Taper le nom que tu veux utiliser pour ce Personnage:");
        String nomPerso = scanIn.nextLine();
        
        Random aleatoire = new Random();
        int x = aleatoire.nextInt(100);
        int y = aleatoire.nextInt(100);
        Point2D pos = new Point2D(x, y);
        
        switch(typePerso){
            case "Archer":
                perso = new Archer(nomPerso, 100, 50, 50, 50, 50, 50, 50, 50, 10, 50, 50, 25, pos);
                break;
            case "Guerrier":
                perso = new Guerrier(nomPerso, 100, 20, 80, 80, 20, 20, 80, 20, 1, 80, 20, pos);
                break;
            case "Mage":
                perso = new Mage(nomPerso, 100, 70, 70, 30, 70, 70, 30, 70, 5, 30, 70, pos);
                break;
            case "Paysan":
                perso = new Paysan(nomPerso, 100, 10, 10, 10, 10, 10, 10, 10, 3, 10, 90, pos);
                break;
        }

        //scanIn.close();  
    }
    /**
     * méthode qui permet au joueur de choisir un action à faire
     * @param monde monde où se trouvent tous les personnages
     */
    public void action(World monde) throws IOException{
        Scanner scanIn = new Scanner(System.in);
        
        System.out.println("Taper le type d'action que tu veux faire dans ce tour de jeu (Combattre/Deplacer/Sauvegarder):");
        String typeAction = scanIn.nextLine();
        
        switch(typeAction){
            case "Combattre":
                this.combattre(monde);
                break;
            case "Deplacer":
                this.deplacer(monde);
                break; 
            case "Sauvegarder":
                 String reponse;
                 String nomFichier = " ";
                 System.out.println("Voulez-vous donner à nom spécifique a votre fichier ? (Tapez Oui ou Non)");
                 reponse= scanIn.nextLine();
                 if (reponse.equals("Oui")) {
                    System.out.println("Tapez le nom du fichier");
                    nomFichier= scanIn.nextLine();
                 }else if(reponse.equals("Non")){
                    int i=0;
                    nomFichier = "sauvegarde" + Integer.toString(i);
                    File f = new File(nomFichier);
                    while (f.exists()) {
                        i++;
                        nomFichier= "sauvegarde" + Integer.toString(i);
                        f = new File(nomFichier);
                    }
                 }
                 if(nomFichier != " "){
                    try {
                       SauvegardePartie sauv =  new SauvegardePartie(nomFichier);
                       sauv.sauvegarderPartie(monde);

                   } catch (IOException ex) {
                       ex.printStackTrace();
                   }
                }
        }
                 
    }
    /**
     * méthode qui permet au personnage de se déplacer
     * @param monde monde où se trouvent tous les personnages
     */
    public void deplacer(World monde){
        int valeurElement = 0;
        Point2D point = new Point2D();
        do{
            Scanner scanIn = new Scanner(System.in);
            System.out.println("Mettez la coordonnée X (nombre de ligne) à laquelle vous voulez déplacer le personnage");
            int x = scanIn.nextInt();
            System.out.println("Mettez la coordonnée Y(nombre de colonne) à laquelle vous voulez déplacer le personnage");
            int y = scanIn.nextInt();
            point.setX(x);
            point.setY(y);
            valeurElement = monde.validerPosition(point);
            if (valeurElement==1){
                System.out.println("Il n'est pas possible de se déplacer a cette case car elle est occupée");
            }
        }while(valeurElement==1 || valeurElement ==6);
        
        switch(valeurElement){
            case -1:
            case 0:
                monde.matriceWorld[this.perso.getPos().getX()][this.perso.getPos().getY()]= 0;
                this.perso.setPos(point);
                System.out.println("Le personnage a été déplacé");
                monde.matriceWorld[point.getX()][point.getY()]= 6;
                break;
            case 2:
                System.out.println("Dans ce case il y avait une potion de vie,votre vie à éte augmenté");
                this.perso.prendrePotVie((Soin)rechercher(point, monde));
                monde.elements.remove(rechercher(point, monde));
                monde.matriceWorld[this.perso.getPos().getX()][this.perso.getPos().getY()]= 0;
                this.perso.setPos(point);
                monde.matriceWorld[point.getX()][point.getY()]= 6;
                break;
            case 3:
                System.out.println("Dans ce case il y avait une potion de mana,votre vie à éte augmenté");
                this.perso.prendrePotMana((Mana)rechercher(point, monde));
                monde.elements.remove(rechercher(point, monde));
                monde.matriceWorld[this.perso.getPos().getX()][this.perso.getPos().getY()]= 0;
                this.perso.setPos(point);
                monde.matriceWorld[point.getX()][point.getY()]= 6;
                break;
            case 4:
                System.out.println("Dans ce case il y avait nourriture");
                this.perso.prendreNourriture((Nourriture)rechercher(point, monde));
                monde.elements.remove(rechercher(point, monde));
                monde.matriceWorld[this.perso.getPos().getX()][this.perso.getPos().getY()]= 0;
                this.perso.setPos(point);
                monde.matriceWorld[point.getX()][point.getY()]= 6;
                break;
    }
    
}

   
    public ElementDeJeu rechercher(Point2D point, World monde){
        for(int i = 0; i < monde.elements.size(); i++){
            if(monde.elements.get(i).getPos().equals(point)){
                return monde.elements.get(i);
            }
        }
        return null;
    }
    
    /**
     * méthode qui permet au personnage de combattre
     * @param monde monde où se trouvent tous les personnages
     */
    public void combattre(World monde){
        Scanner scanIn = new Scanner(System.in);
        System.out.println("mettez la coordonnée X (nombre de ligne) avec laquelle vous voulez vous combattre");
        int x = scanIn.nextInt();
        System.out.println("mettez la coordonnée Y (nombre de colonne) avec laquelle vous voulez vous combattre");
        int y = scanIn.nextInt();
        //no lo encuentra
        Point2D point = new Point2D(x, y);
        Creature c = (Creature) this.rechercher(point, monde);
        
        if(monde.matriceWorld[x][y] == 1){
            switch(typePerso){
                case "Archer":
                    ((Archer) perso).combattre(c);
                    break;
                case "Guerrier":
                    ((Guerrier) perso).combattre(c);
                    break;
                case "Mage":
                    ((Mage) perso).combattre(c);
                    break;
                case "Paysan":
                    System.out.println("Votre classe n'est permett pas de combattre!");
                    break;
            }
        }
        else{
            System.out.println("Pas de personnage dans cette position! Impossible de Combattre!");
        }
    }
}
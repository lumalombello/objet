/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Luma et Luz
 */

public class TestSeance {
    
    /**
     * @param argv
     */
    
    
    public static void main(String[] argv) throws IOException{    
        // TODO code application logic here
        
        //Debut d'une partie
        System.out.println("Voici les légendes du plateu de jeu: ");
        System.out.println("00: case vide , Jo: joueur, Ar: archer, Gu: guerrier, Ma: mage, Pa: paysan, ");
        System.out.println("Lo: loup, La: lapin , So: soin, Mn: mana, No: nourriture, To: nuageToxique\n");
         
        System.out.println("Voulez-vous charger une partie existante? (Oui/Non)");
        
        Scanner scanIn = new Scanner(System.in);
        String continuerPartie = scanIn.nextLine();
        World monde = new World();
        
        if(continuerPartie.equals("Oui")){
            System.out.println("Tapez le nom du fichier où la partie est sauvegarde");
            String fichier = scanIn.nextLine();
            ChargementPartie charge = new ChargementPartie(fichier);
            monde = charge.chargerPartie();
        }
        else if(continuerPartie.equals("Non")){
            // Faire les tours comme on faisait avant
            monde.creeMondeAlea();
            monde.creationJoueur();
        }
        
        String partie = "Oui";
        int i = 1;
        while(!partie.equals("Non")){
            System.out.println("TOUR" + i);
            monde.tourDeJeu();
            System.out.println("Continuer partie (Oui/Non)?");
            partie = scanIn.nextLine();
            i++;
        }

    } 
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *
 * @author luma
 */
public class ChargementPartie {
    
    
    //Attributs
    protected String source;
    BufferedReader fichier ;
    
    /**
     * Constructeur
     * @param source 
     */
    public ChargementPartie(String source) {
        this.source = source;
    }

    /**
     * Methode qui reçoit un fichier et genere les Personnages de World 
     * @return World
     */
  public World chargerPartie() { 
    World woe = new World();
    ElementDeJeu element;
    
    try {
        
      String ligne ;
      ArrayList<String> caracteristiques = new ArrayList<>();
      fichier = new BufferedReader(new FileReader(source));
      ligne = fichier.readLine();
      
      while (ligne != null) {
          
        String delimiteurs = " ,.;";
        StringTokenizer tokenizer = new StringTokenizer(ligne, delimiteurs);
        
        while(tokenizer.hasMoreTokens()) {
          String mot = tokenizer.nextToken();
          mot = mot.toLowerCase();
          caracteristiques.add(mot);
        }
        
        if(caracteristiques.get(0).equals("dimension")){ 
            //Initialization matrice world
            woe.setDimension(Integer.parseInt(caracteristiques.get(1)));
            int matrice[][] = new int[Integer.parseInt(caracteristiques.get(1))][Integer.parseInt(caracteristiques.get(1))];
            woe.setMatriceWorld(matrice);
        }else if(caracteristiques.get(0).equals("joueur")){
            int X, Y;
            Point2D pos = new Point2D(Integer.parseInt(caracteristiques.get(caracteristiques.size()-2)), Integer.parseInt(caracteristiques.get(caracteristiques.size()-1)));
            Joueur ju = new Joueur(caracteristiques, pos);
            woe.j = ju;
            X=ju.getPerso().getPos().getX();
            Y=ju.getPerso().getPos().getY();
            woe.matriceWorld[X][Y] = 6;
            
        }else{  
            //Creation des elements de jeu
            
            element = this.creerElementJeu(caracteristiques);
            if (element != null){ //error
                int valeur, coordX, coordY;
                valeur = this.obtenirValeur(element);
                coordX=element.getPos().getX();
                coordY=element.getPos().getY();
                woe.elements.add(element);
                woe.matriceWorld[coordX][coordY] = valeur;
            }
        }

        ligne = fichier.readLine();
        caracteristiques.removeAll(caracteristiques);

      }
      fichier.close();
    } catch (Exception e) {
      e.printStackTrace();
    }  
    return woe;
  }  
  
  /**
   * Methode qui reçoit les caracteristiques d'un ElementDeJeu et le cree
   * @param caracteristiques
   * @return ElementDeJeu
   */
  public ElementDeJeu creerElementJeu(ArrayList<String> caracteristiques){
      
      ElementDeJeu element;
      Point2D pos = new Point2D(Integer.parseInt(caracteristiques.get(caracteristiques.size()-2)), Integer.parseInt(caracteristiques.get(caracteristiques.size()-1)));
      
      switch (caracteristiques.get(0)){
                   
                  case "archer":
                      element = new Archer(caracteristiques.get(1), Integer.parseInt(caracteristiques.get(2)), +
                              + Integer.parseInt(caracteristiques.get(3)), Integer.parseInt(caracteristiques.get(4)), +
                              + Integer.parseInt(caracteristiques.get(5)), Integer.parseInt(caracteristiques.get(6)), +
                              + Integer.parseInt(caracteristiques.get(7)), Integer.parseInt(caracteristiques.get(8)), +
                              + Integer.parseInt(caracteristiques.get(9)), Integer.parseInt(caracteristiques.get(10)), +
                              + Integer.parseInt(caracteristiques.get(11)), Integer.parseInt(caracteristiques.get(12)), +
                              + Integer.parseInt(caracteristiques.get(13)), pos);
                      break;
                  case "bonuspourcentageatt":
                      element = new BonusPourcentageAtt(Integer.parseInt(caracteristiques.get(1)), Integer.parseInt(caracteristiques.get(2)), pos);
                      break;
                  case "guerrier":
                      element = new Guerrier(caracteristiques.get(1), Integer.parseInt(caracteristiques.get(2)), +
                              + Integer.parseInt(caracteristiques.get(3)), Integer.parseInt(caracteristiques.get(4)), +
                              + Integer.parseInt(caracteristiques.get(5)), Integer.parseInt(caracteristiques.get(6)), +
                              + Integer.parseInt(caracteristiques.get(7)), Integer.parseInt(caracteristiques.get(8)), +
                              + Integer.parseInt(caracteristiques.get(9)), Integer.parseInt(caracteristiques.get(10)), +
                              + Integer.parseInt(caracteristiques.get(11)), Integer.parseInt(caracteristiques.get(12)), pos);
                      break;
                  case "lapin":
                      element = new Lapin(caracteristiques.get(1), Integer.parseInt(caracteristiques.get(2)), +
                              + Integer.parseInt(caracteristiques.get(3)), Integer.parseInt(caracteristiques.get(4)), +
                              + Integer.parseInt(caracteristiques.get(5)), Integer.parseInt(caracteristiques.get(6)), +
                              + Integer.parseInt(caracteristiques.get(7)), pos);
                      break;
                  case "loup":
                      element = new Loup(caracteristiques.get(1), Integer.parseInt(caracteristiques.get(2)), +
                              + Integer.parseInt(caracteristiques.get(3)), Integer.parseInt(caracteristiques.get(4)), +
                              + Integer.parseInt(caracteristiques.get(5)), Integer.parseInt(caracteristiques.get(6)), +
                              + Integer.parseInt(caracteristiques.get(7)), pos);
                      break;
                  case "mage":
                      element = new Mage(caracteristiques.get(1), Integer.parseInt(caracteristiques.get(2)), +
                              + Integer.parseInt(caracteristiques.get(3)), Integer.parseInt(caracteristiques.get(4)), +
                              + Integer.parseInt(caracteristiques.get(5)), Integer.parseInt(caracteristiques.get(6)), +
                              + Integer.parseInt(caracteristiques.get(7)), Integer.parseInt(caracteristiques.get(8)), +
                              + Integer.parseInt(caracteristiques.get(9)), Integer.parseInt(caracteristiques.get(10)), +
                              + Integer.parseInt(caracteristiques.get(11)), Integer.parseInt(caracteristiques.get(12)), pos);
                      break;
                  case "maluspourcentageatt":
                      element = new MalusPourcentageAtt(Integer.parseInt(caracteristiques.get(1)), Integer.parseInt(caracteristiques.get(2)), pos);
                      break;
                  case "mana":
                      element = new Mana(Integer.parseInt(caracteristiques.get(1)), pos);
                      break;
                  case  "nuagetoxique":
                      element = new NuageToxique(Integer.parseInt(caracteristiques.get(1)), pos);
                      break;
                  case "paysan":
                      element = new Paysan(caracteristiques.get(1), Integer.parseInt(caracteristiques.get(2)), +
                              + Integer.parseInt(caracteristiques.get(3)), Integer.parseInt(caracteristiques.get(4)), +
                              + Integer.parseInt(caracteristiques.get(5)), Integer.parseInt(caracteristiques.get(6)), +
                              + Integer.parseInt(caracteristiques.get(7)), Integer.parseInt(caracteristiques.get(8)), +
                              + Integer.parseInt(caracteristiques.get(9)), Integer.parseInt(caracteristiques.get(10)), +
                              + Integer.parseInt(caracteristiques.get(11)), Integer.parseInt(caracteristiques.get(12)), pos);
                      break;
                  case "soin":
                      element = new Soin(Integer.parseInt(caracteristiques.get(1)), pos);
                      break;
                  default:
                      element = null;
              }
      
      return element;
  }
  
  
  public int obtenirValeur(ElementDeJeu e){
      int valeur=7;
      if(e instanceof Creature){
          valeur =1;
      }else if(e instanceof Soin){
          valeur =2; 
     }else if(e instanceof Mana){
          valeur =3; 
     }else if(e instanceof Nourriture){
         valeur=4;
     }else if(e instanceof NuageToxique){
         valeur=5;
    } 
      return valeur;
  }
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 *
 * @author Luma et Luz
 */
public class Objet extends ElementDeJeu{
    
    /**
     * Attributs
     */
    
    /**
     * Constructeur vide
     */
    
    public Objet() {
        
    }
    
    /**
     * Constructeur de recopie
     * @param o instance de la classe Objet
     */
    
    public Objet(Objet o) {
        super(o.getPos());
    }
    
    /**
     * Constructeur de la classe Objet
     * @param pos position
     */
    
    public Objet(Point2D pos) {
        super (pos);
    }
    
   
}